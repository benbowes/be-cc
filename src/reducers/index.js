import { combineReducers } from 'redux';

import { races } from './races';

const rootReducer = combineReducers({
  races,
});

export { rootReducer };