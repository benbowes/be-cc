import * as actionTypes from '../constants/actionTypes';

/**
 * Application defaults - will overridden at store creation time
 */
const initialState = {
  pollTime: 30000,
  endpoint: '',
  results: []
};

export function races(state = initialState, action) {
  switch (action.type) {
    case actionTypes.REQUEST_RACES:
      return {
        ...state,
        loading: true,
      }

    case actionTypes.RECEIVE_RACES:
      return {
        ...state,
        loading: false,
        results: action.value.map(result => ({
          eventTypeDesc: result.EventTypeDesc,
          eventName: result.EventName,
          advertisedStartTime: result.AdvertisedStartTime,
          venue: result.Venue
        }))
      };

    case actionTypes.REQUEST_RACES_FAIL:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}