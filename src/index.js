import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { App } from './App';
import { configureStore } from './store';

import './index.scss'

/**
 * pass the initial config/state in
 */
const store = configureStore({
  races: {
    endpoint: 'https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump',
    pollTime: 10000,
    results: [],
    loading: true,
  }
});

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
