import React from 'react';
import { connect } from 'react-redux';
import { BetFairLogo } from './components/BetFairLogo';
import { Loader } from './components/Loader';
import { Results } from './components/Results';

import './App.scss';

const AppComponent = ({ loading, results }) => {
  return (
    <div>
      <header className="header">
        <BetFairLogo />
        <Loader loading={loading} />
      </header>
      <main>
        <Results results={results} />
      </main>
      {/* <pre>{JSON.stringify(results, null, 2)}</pre> */}
    </div>
  );
}

export const App = connect(state => ({
  results: state.races.results,
  loading: state.races.loading
}))(AppComponent);
