import { mount } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";

import { App } from "./App";
import { configureStore } from "./store";

function setup(initialState) {
  const store = configureStore(initialState);
  return {
    component: mount(
      <Provider store={store}>
        <App />
      </Provider>
    ),
    store,
  };
}

describe('App', () => {
  it('Loads data', () => {
    const { component } = setup({
      races: {
        endpoint: 'https://goodResponse',
        pollTime: 1000,
        results: [],
        loading: true,
      }
    });

    process.nextTick(() => {
      component.update();
      expect(component.find('.race__event-location__name').length).toEqual(5);
    })
  });

  it('Fails as expected when bad data', () => {
    const consoleWarnSpy = jest.spyOn(console, "warn");
    const { component } = setup({
      races: {
        endpoint: 'https://badResponse',
        pollTime: 1000,
        results: [],
        loading: true,
      }
    });

    process.nextTick(() => {
      component.update();
      expect(consoleWarnSpy.mock.calls[0][0]).toEqual("ERROR"); // not great
      expect(component.find('.race__event-location__name').length).toEqual(0)
    })
  });
});