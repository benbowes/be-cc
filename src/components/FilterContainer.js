import React from 'react';
import './FilterContainer.scss';

export const FilterContainer = ({ children }) => (
  <div className="filter-container">
    {children}
  </div>
);