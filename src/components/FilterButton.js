import React from 'react';
import { getRaceTypeIcon } from './raceTypeIcons';

import './FilterButton.scss';

export const FilterButton = ({ eventType, selected, onSelect }) => {
  if (!eventType) {
    return (
      <button onClick={() => onSelect('')} className={`filter-button ${selected === '' || !selected ? 'filter-button--selected' : ''}`}>
        All
      </button>
    );
  }

  return (
    <button onClick={() => onSelect(eventType)} className={`filter-button ${selected === eventType ? 'filter-button--selected' : ''}`}>
      {getRaceTypeIcon(eventType)}
    </button>
  );
}