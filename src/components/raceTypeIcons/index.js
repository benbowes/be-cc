import React from 'react'
import * as eventTypes from '../../constants/eventTypes'
import { Thoroughbred } from './Thoroughbred';
import { Greyhound } from './Greyhound';
import { Trots } from './Trots';

function getRaceTypeIcon(raceType) {
  switch (raceType) {
    case eventTypes.Trots:
      return <Trots />;

    case eventTypes.Greyhounds:
      return <Greyhound />;

    case eventTypes.Thoroughbred:
      return <Thoroughbred />;

    default:
      return null;
  }
}

export { getRaceTypeIcon, Thoroughbred, Greyhound, Trots }