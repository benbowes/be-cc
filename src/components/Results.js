import React from 'react'
import { Race } from './Race';
import { FilterContainer } from './FilterContainer';
import { FilterButton } from './FilterButton';
import * as eventTypes from '../constants/eventTypes'

export class Results extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterBy: undefined,
      filteredResults: props.results,
    }
  }

  onSelect = (eventType) => {
    const { results } = this.props;

    this.setState({
      filterBy: eventType,
      filteredResults: eventType === ''
        ? results
        : results.filter(result => result.eventTypeDesc === eventType)
    })
  }

  render() {
    const { filterBy, filteredResults } = this.state;
    const { results } = this.props;
    const raceResults = (filterBy === undefined ? results : filteredResults);

    return (
      <>
        <FilterContainer>
          <FilterButton onSelect={this.onSelect} selected={filterBy} />
          <FilterButton onSelect={this.onSelect} selected={filterBy} eventType={eventTypes.Thoroughbred} />
          <FilterButton onSelect={this.onSelect} selected={filterBy} eventType={eventTypes.Trots} />
          <FilterButton onSelect={this.onSelect} selected={filterBy} eventType={eventTypes.Greyhounds} />
        </FilterContainer>

        {raceResults.map((result, index) => {
          return <Race key={`result_${index}`} result={result} />
        })}
      </>
    );
  }
}