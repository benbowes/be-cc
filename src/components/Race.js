import React from 'react';
import moment from 'moment';
import { getRaceTypeIcon } from './raceTypeIcons'

import './Race.scss';

export const Race = ({ result }) => (
  <a className="race" href="http://betfair.com.au">
    <div className="race__type-icon">{getRaceTypeIcon(result.eventTypeDesc)}</div>
    <div className="race__event-location">
      <div className="race__event-location__name">{result.eventName}</div>
      <div className="race__event-location__venue">{result.venue.Venue}</div>
    </div>
    <div className="race__event-time">{moment(result.advertisedStartTime).fromNow()}</div>
  </a>
)