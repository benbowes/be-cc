import { call, select } from 'redux-saga/effects';
import { get } from '../api';

export function* getRaces() {
  while (true) {
    const endpoint = yield select(state => state.races.endpoint);
    const response = yield call(get, { endpoint });
    return response;
  }
}
