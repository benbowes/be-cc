import { delay } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { getRaces } from './tasks/getRaces';
import * as actionTypes from '../constants/actionTypes';

export function* bootstrap() {
  const { pollTime } = yield select(state => state.races);

  while (true) {
    yield put({ type: actionTypes.REQUEST_RACES });

    const result = yield call(getRaces);

    if (result.ok) {
      yield put({ type: actionTypes.RECEIVE_RACES, value: result.response });
    } else {
      console.warn('ERROR', result);
      yield put({ type: actionTypes.REQUEST_RACES_FAIL });
    }

    // initiate simple polling on this saga
    yield call(delay, pollTime);
  }
}

export function* rootSaga() {
  yield call(bootstrap);
}