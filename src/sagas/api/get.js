import axios from 'axios';

/**
 * @param {String} endpoint - a url
 */
export function get({ endpoint }) {
  return axios.get(endpoint)
    .then((response) => {
      if (!response.status >= 400) {
        throw response;
      } else {
        return {
          ok: true,
          response: response.data.result,
        }
      }
    })
    .catch(error => {
      return {
        ok: false,
        error,
        response: { data: { result: [] } }
      };
    });
}