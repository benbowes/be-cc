import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { rootReducer } from '../reducers';
import { rootSaga } from '../sagas/index';

/*
 * @returns {Object} redux store
 * @description `rootSaga*` will is called after middleware is applied.
 * `Redux devToolsExtension` Chrome extension will be available if yer `Chrome` has it.
 */
export function configureStore({ races }) {
  const sagaMiddleware = createSagaMiddleware();

  const initialStoreState = { races };

  const store = createStore(
    rootReducer,
    initialStoreState,
    compose(
      applyMiddleware(sagaMiddleware),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );

  sagaMiddleware.run(rootSaga);

  if (!window.devToolsExtension) {
    console.log(`Download Redux DevTools for a better dev experience\n
      https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd
    `);
  }

  return store;
}