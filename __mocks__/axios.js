
import { results1 } from "./mockResponses/results1";
import { results2 } from "./mockResponses/results2";

let moduloCount = 0;

export default {
  get: jest.fn(url => {
    switch (url) {
      case 'https://goodResponse':
        moduloCount++;
        return Promise.resolve([results1, results2][moduloCount % 2]);

      case 'https://badResponse':
        return Promise.resolve({ status: 500 });

      default:
        throw new Error(`AXIOS MOCK ERROR!!! Please setup a url for ${url} in /__mocks__/axios.js`);
    }
  }),
};
