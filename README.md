# Code challenge

This code challenge is running off create-react-app - an ejected version.

### Running the website

Ensure that you have a reasonably new node and npm version https://nodejs.org/en/,
Then in your terminal, navigate to the root of this project and run:
- `yarn install` or `npm i`
- `yarn start` or `npm run start`

### Tests

I do really enjoy writting tests. For this challenge, the tests are limited. I would normally write tests at an interaction level, a data integration level and to a lesser degree jest snapshots. As a codebase gets bigger, having tests becomes increasingly important as it gives your co-workers more confidence that they are not breaking things unintentionally.

**Running the test suite**
- `yarn test` or `npm run test`

#Todo

- A lot more tests
- Add Prettier